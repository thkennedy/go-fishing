﻿using UnityEngine;
using System.Collections;

public class Table : MonoBehaviour {

    public float m_fHeight;
    public float m_fWidth;
    public Vector3 m_vPosition;

    private Resolution[] m_CurrentScreenResolution;

	// Use this for initialization
	void Start () 
    {
        // allow to rotate to landscape in either direction
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        
        // do not allow to go to portrait
        Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;

        Screen.orientation = ScreenOrientation.AutoRotation;
        // init the table
        SetTableToScreenResolution();

	    // Store current resolution
        m_CurrentScreenResolution = Screen.GetResolution;
	}
	
	// Update is called once per frame
	void Update () 
    {
        // see if resolution has changed
        if (m_CurrentScreenResolution != Screen.GetResolution)
            SetTableToScreenResolution();

        // update screen orientation
        Screen.orientation = ScreenOrientation.AutoRotation;
	}

    void SetTableToScreenResolution()
    {
        // center the table
        transform.localPosition = m_vPosition;

        // Set the table size to the screen resolution
        m_fHeight = Camera.main.orthographicSize * 2.0f;
        m_fWidth = m_fHeight * Screen.width / Screen.height;
        transform.localScale = new Vector3(m_fWidth , m_fHeight,1 );
    }
}
