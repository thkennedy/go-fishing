﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bitrave.Azure;
using Parse;
using System.Threading.Tasks;
using System;
using System.Reflection;

public class MainGameLogic : MonoBehaviour {

    // Azure Mobile Services 
    private AzureMobileServices MobileServices;

    // facebook manager
    public FacebookManager FBManager;

    // deck prefab
    public DeckStandard StandardDeckPrefab;

    // game prefab
    public GoFishGame GamePrefab;

    // list of games I'm playing
    public List<GoFishGame> m_lGameList = new List<GoFishGame>();

    // recheck rate for waiting on services to finish
    public const float APP_SERVICE_REFRESH_RATE = 1.0f;

    // letting outside entities know if app is busy
    public bool m_bAppIsBusy;

	// This is a temp container for query results
	public IEnumerable<ParseObject> QueryResults;

    private bool bGameListNeedsUpdating;

    public bool bIsInGame;

    public bool bIsViewingGameList;

    public string PlayerID;

    public float m_fButtonHeight;

    public float m_fButtonWidth;

    public float m_fButtonSpacing;

    public float m_fScreenWidth;

    public float m_fScreenHeight;

    

    void OnGUI()
    {
        // if we're not in game or viewing games, show the main menu options
        if (!bIsInGame && !bIsViewingGameList)
        {
            // calc sizes
            var Width = m_fScreenWidth * m_fButtonWidth;
            var Height = m_fScreenHeight * m_fButtonHeight;
            var TopLeftX = m_fScreenWidth / 4 - Width / 2;
            var TopLeftY = m_fScreenHeight / 4.0f - Height / 2.0f;


            if (GUI.Button(new Rect(TopLeftX, TopLeftY, Width, Height), "Show Games Playing"))
            {
                RefreshGamesPlaying();

                bIsViewingGameList = true;
            }

            TopLeftY += Height + m_fButtonSpacing;

            if (GUI.Button(new Rect(TopLeftX, TopLeftY, Width, Height), "Find Open Games"))
            {
                FindOpenGames();

                bIsViewingGameList = true;
            }

            TopLeftY += Height + m_fButtonSpacing;
            if (GUI.Button(new Rect(TopLeftX, TopLeftY, Width, Height), "Create New Game"))
            {
                // create new game
                CreateNewGame();

                // refresh game list
                RefreshGamesPlaying();

                bIsViewingGameList = true;
            }
            // only show FB icon if not logged in
            if (!FB.IsLoggedIn)
            {
                if (GUI.Button(new Rect(0, 0, FBManager.FBButton.width, FBManager.FBButton.height), FBManager.FBButton))
                {
                    FBManager.Login();

                    //StartCoroutine(WaitForFBToLogin());

                }
            }

            
        } // end if (bIsInGame)
        // show game list
        else if (!bIsInGame)
        {
            int NumGames = 5;

            var Width = m_fScreenWidth * m_fButtonWidth;
            var Height = m_fScreenHeight * m_fButtonHeight;
            var TopLeftX = 0.0f;
            var TopLeftY = 0.0f;
            

            if (GUI.Button(new Rect(0, 0, Width, Height), "Return to Main Menu"))
            {
                bIsViewingGameList = false;
                return;

            }
            // display games from selected above
            if (m_lGameList.Count > 0)
            {
                // re- calc sizes
                Width = m_fScreenWidth * m_fButtonWidth;
                Height = m_fScreenHeight * m_fButtonHeight;
                TopLeftX = m_fScreenWidth / 5.0f - Width / 2.0f;
                TopLeftY = m_fScreenHeight / 5.0f - Height / 2.0f + 20;

                int X = 0;
                int Y = 0;
                

                for (int i = 1; i <= m_lGameList.Count; i++)
                {
                    if (GUI.Button(new Rect(TopLeftX + ((Width + m_fButtonSpacing) * X), TopLeftY + ((Height + m_fButtonSpacing) * Y), Width, Height), m_lGameList[i-1].Id))
                    {
                        // this is where I will enter the game
                        // join the game (if I'm already a part of it, will early out)
                        GoFishGame SelectedGame = m_lGameList[i-1];
                        JoinGame(ref SelectedGame);
                        // let menu know in I'm in a game now
                        bIsInGame = true;
                        // let game know to take control
                        m_lGameList[i-1].bGameIsActive = true;

                    } // end button

                    // increment Y
                    ++Y;


                    // if i is a mult of NumGames, increment the column
                    if (i % NumGames == 0)
                    {
                        ++X;
                        Y = 0;
                    }
                }
            } // end gamelist
            else
            {

            }
        }
    }

    // coroutine to continually try to initialize Azure
    IEnumerator WaitForFBToLogin()
    {
        while (true)
        {
            if (FBManager.bIsBusy)
            {
                Debug.Log("Waiting on FB to log in");
                yield return new WaitForSeconds(2);
                PlayerID = "NOT LOGGED INTO FB";
            }
            else if (FB.IsLoggedIn)
            {
                Debug.Log("FB Logged in, getting UID");
                PlayerID = FB.UserId;
                yield break;
            }
            else
            {
                Debug.Log("Failed to log in, try again");

            }
        }
    }

	// Use this for initialization
	void Start () 
    {


	
		bGameListNeedsUpdating = false;
        bIsInGame = false;

        FBManager.MainGameVars = this;

        // default scales for buttons and spacings
        m_fButtonWidth = 0.20f;
        m_fButtonHeight = 0.15f;
        m_fButtonSpacing = 10;

        // store screen width and height
        m_fScreenHeight = Screen.height;
        m_fScreenWidth = Screen.width;



        
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        // update game list if necessary
        if (bGameListNeedsUpdating)
        {
            UpdateGameList();
        }

        // update button sizings

	
	}

    void CleanUpGameData()
    {
        Debug.Log("Cleaning Up Game Data");
        foreach (var Game in m_lGameList)
        {
            Destroy(Game.gameObject,1);
        }

        m_lGameList.Clear();
    }

    IEnumerator WaitForActionToFinish()
    {
        while (true)
        {
            Debug.Log("Waiting for Service to finish.");
            // if service isn't busy, its done so move on
            if (true)
            {
                m_bAppIsBusy = false;
                yield break;
            }

            // TODO: DISABLE UI AND DO LOADING LOGIC HERE

            //yield return new WaitForSeconds(APP_SERVICE_REFRESH_RATE);
        }
    }

    public void FindOpenGames()
    {
        // mark app as busy
        m_bAppIsBusy = true;

        // query database for number of entries where looking for more players is
        // set to true
        var Query = ParseObject.GetQuery("GoFishGame")
            .WhereEqualTo("WantsMorePlayers", true);
        
        Query.CountAsync().ContinueWith(t =>
        {
            int count = t.Result;

            if (count > 0)
            {
                Debug.Log("Found games looking for more players...");

                // found records, get them
                Query.FindAsync().ContinueWith(task =>
                {
                    // dump data back into main app so I can parse the games
                    QueryResults = task.Result;

                    // let system know to update game list
                    bGameListNeedsUpdating = true;


                }); // end findasync
            } // end if 
            else
            {
                Debug.Log("No Open Games found - create one!");

                QueryResults = null;

                bGameListNeedsUpdating = true;
            }

            m_bAppIsBusy = false;
        });// end query
    }

    public void RefreshGamesPlaying()
    {
		// mark app as busy
		m_bAppIsBusy = true;

        // query database for number of entries where I am listed as a player
        var Query = ParseObject.GetQuery("GoFishGame")
            .WhereEqualTo("Players", PlayerID);

        //var Query = ParseObject.GetQuery("GoFishGame")
        //   .WhereEqualTo("Players", "noone");

        Query.CountAsync().ContinueWith(t =>
        {
            int count = t.Result;

            if (count > 0)
            {
                Debug.Log("Found games you are a part of, retrieving...");
                
                // found records, get them
                Query.FindAsync().ContinueWith(task =>
                {
					// dump data back into main app so I can parse the games
                    QueryResults = task.Result;

                    // let system know to update game list regardless of outcome
                    bGameListNeedsUpdating = true;
                    // app no longer busy so resume
                    m_bAppIsBusy = false;
                                        
                }); // end findasync
            } // end if 
			else
			{
				Debug.Log("No Games Found, sorry!");

			}

            // let system know to update game list regardless of outcome
            bGameListNeedsUpdating = true;
            // app no longer busy so resume
			m_bAppIsBusy = false;
        });// end query

        
    }


	/** 
	 * Creates game list based on QueryResults
	 * */
	public void UpdateGameList()
	{

        Debug.Log("Updating Game List");

		GoFishGame tempGame;

        // whatever we do, going to rebuild the game list
        CleanUpGameData();
        
        // if queryresults is null, I've either set it due to no games found
        // or something bad happened, so bail
        if (QueryResults == null)
        {
            // no games found, delete game list
            m_lGameList.Clear();
            
            // game list updated, no longer needs updating
            bGameListNeedsUpdating = false;

            

            return;
        }

        // loop through until count
        foreach (var result in QueryResults)
        {

            // create a new gofish game
            tempGame = Instantiate(GamePrefab) as GoFishGame;
            tempGame.Init();

            // give reference to main menu
            tempGame.MainGameVars = this;

            // convert to net data
             tempGame.NetData.FromParseObject(result);

            // convert from net data to game data
            tempGame.GetNetData();

            // add game to list
            m_lGameList.Add(tempGame);



        }

        // game list updated, no longer needs updating
        bGameListNeedsUpdating = false;

		// clear query results - don't want old data
		QueryResults = null;

        
	}

    public void CreateNewGame()
    {
		// mark app as busy
		m_bAppIsBusy = true;

        // create new game
        GoFishGame NewGame = Instantiate(GamePrefab) as GoFishGame;

        NewGame.Init();

        // set beginning vars
        // give it a reference to the main game
        NewGame.MainGameVars = this;
        // give game a semi-unique id based on device + game number - may get rid of this later
        NewGame.GameID = PlayerID + m_lGameList.Count;
        // host of the game - will later use name/fb/etc + this identifier
        NewGame.Host = PlayerID;
        // player turn will be the person who joins after the host - however, can't assign it
        // to someone who isn't here yet - set to host for now. When game begins will rand first player
        NewGame.PlayerTurn = NewGame.Host;

        // create deck
        DeckStandard CardDeck = Instantiate(StandardDeckPrefab) as DeckStandard;

        CardDeck.Initialize();

		NewGame.DeckCards = CardDeck;
        // shuffle the deck
        NewGame.DeckCards.Shuffle();
        // hands haven't been dealt yet, but create container
        NewGame.PlayerHands = new Dictionary<string, CardHand>();

        // set number of tricks for host
        NewGame.PlayerTricks = new Dictionary<string, int>();
        // set game started to false
        NewGame.bIsGameStarted = false;
        // set numplayers to 1 
        NewGame.NumPlayers = 1;
        // set game to allow more players by default
        NewGame.WantsMorePlayers = true;
        // set game state to waiting for players
        NewGame.CurrentGameState = GameState.eWaitingForPlayers;

        // join the game we just made
        // add self to players list
        NewGame.Players.Add(PlayerID);

        // create a player hand and add to game
        CardHand NewPlayerHand = new CardHand();


        NewGame.PlayerHands.Add(PlayerID, NewPlayerHand);

        // set serializable data
        NewGame.SetNetData();

        Task saveTask = NewGame.NetData.ToParseObject("GoFishGame").SaveAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                // the save was cancelled.
                Debug.Log("Save cancelled.");
            }
            else if (task.IsFaulted)
            {
                AggregateException exception = task.Exception;
                Debug.Log(exception);
            }
            else
            {
                // the object was saved successfully.
                Debug.Log("Save Successful");
                //Save ID
                NewGame.Id = NewGame.NetData.GetID();
                Debug.Log("Game Unique ID from Parse is: " + NewGame.Id);
                
            }

			m_bAppIsBusy = false;

            // update game list as we're going to get it again from server
            bGameListNeedsUpdating = true;
            
        });
        
        // destroy local object created here as its a duplicate now
        Destroy(NewGame.gameObject, 1);

    }

    // UpdateGame
    public void UpdateGame()
    {



    }
    // Search For Open Games
    public void SearchOpenGames()
    {

    }
    // join a game that is currently open
    public void JoinGame(ref GoFishGame GameToJoin)
    {
        // see if I'm already playing this game
        foreach (var Player in GameToJoin.Players)
        {

            if (Player == PlayerID)
            {
                Debug.Log("Already a player of this game, not joining again.");
                return;
            }
        }

        // didn't find myself in playerlist, join the game
        Debug.Log("Doesn't look like I'm a part of this game, will join.");

        // add self to players list
        GameToJoin.Players.Add(PlayerID);

        // create a player hand and add to game
        CardHand NewPlayerHand = new CardHand();
               
        // add new hand
        GameToJoin.PlayerHands.Add(PlayerID, NewPlayerHand);

        // add to number of players
        GameToJoin.NumPlayers++;

        // if I'm the game's sixth player, turn off find games and start 
        if (GameToJoin.NumPlayers >= 6)
        {
            Debug.Log("6th Player Joined, no more allowed");
            GameToJoin.WantsMorePlayers = false;
            GameToJoin.CurrentGameState = GameState.eGameFullWaitingToStart;
        }

        // sends game data to netdata
        GameToJoin.SetNetData();

       

        Task saveTask =  GameToJoin.NetData.ToParseObject("GoFishGame").SaveAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                // the save was cancelled.
                Debug.Log("Save cancelled.");
            }
            else if (task.IsFaulted)
            {
                AggregateException exception = task.Exception;
                Debug.Log(exception);
            }
            else
            {
                // the object was saved successfully.
                Debug.Log("Save Successful");
               
            }

            m_bAppIsBusy = false;
        });
                
    }
}


