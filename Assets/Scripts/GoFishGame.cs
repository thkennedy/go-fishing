﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using Newtonsoft.Json;
using Linq2Rest.Provider;
using Bitrave.Azure;
using Parse;
using System.Reflection;


public enum GameState
{
    eWaitingForPlayers,
    eGameFullWaitingToStart,
    eGameStarted,
    eGameFinished
};
/** 
 * GoFishGame Class
 * Needs to have matching columns for all vars in Azure
 * Types can be int, bool, float, string */
public class GoFishGame : MonoBehaviour
{
    // prefabs
    public DeckStandard Deckprefab;
	
    // Net Data - serializable data class
    public GoFishGameNetData NetData;

    // azure id of the game
    public string Id;

    // unique id of the game based on host name and uniqueid
    public string GameID;

    // host of the game
    public string Host;

    // all players of the game - limiting to 6 for now
    public IList<string> Players;

    // who's turn is it 
    public string PlayerTurn;

    //what's left in the deck
    public DeckStandard DeckCards;

    // what cards everyone has in their hand
    public IDictionary<string, CardHand> PlayerHands;

    // scoring - who has how many tricks
    public IDictionary<string, int> PlayerTricks;

    // game started or not
    public bool bIsGameStarted;

    // number of players in a game
    public int NumPlayers;

    // unique deck ID 
    public string DeckID ;

    // determines if game is looking for more players
    public bool WantsMorePlayers;
    
    //public CardAtlas Atlas ;

    //public CardStock Stock;

    public bool bGameIsActive;

    // a reference to the main game for access to vars
    public MainGameLogic MainGameVars;

    public GameState CurrentGameState;

    //public ParseObject ParseData;


    

    public void Init()
    {
        Players = new List<string>();
        NetData = new GoFishGameNetData();
        // create deck
		DeckCards = Instantiate(Deckprefab) as DeckStandard;
		PlayerHands = new Dictionary<string, CardHand>();
		PlayerTricks = new Dictionary<string, int>();
        bGameIsActive = false;


    }

    void OnDestroy()
    {
        Debug.Log("Destroying GoFishGame Object");

        // destroy cards in hand
        foreach (var entry in PlayerHands)
        {
            for (int i = 0; i < entry.Value.m_lCardsInHand.Count; ++i)
            {
                if (entry.Value.m_lCardsInHand[i] != null)
                    Destroy(entry.Value.m_lCardsInHand[i].gameObject, 1);
            }
        }

        // destroy deck
        if (DeckCards != null)
            Destroy(DeckCards.gameObject,1);

        // clear entries
        PlayerHands.Clear();
        // set to null so GC collects
        PlayerHands = null;

        // clear tricks
        PlayerTricks.Clear();
        PlayerTricks = null;

        // clear netdata
        NetData = null;

        // clear Players
        Players.Clear();
        Players = null;


    }


    public static string DictToString<T, V>(IEnumerable<KeyValuePair<T, V>> items, string format)
    {
        format = string.IsNullOrEmpty(format) ? "{0};{1}$" : format;

        StringBuilder itemString = new StringBuilder();
        foreach (var item in items)
        {
            // if I can't cast to a string, call ToString to make it one
            itemString.AppendFormat(format, item.Key, item.Value.ToString());

        }

        return itemString.ToString();
    }

    // sets netdata from game object overwriting values
    public void SetNetData()
    {
        // only set ID if its an update
        if (NetData.Id != null)
            NetData.Id = Id;

        NetData.GameID = GameID;
        NetData.Host = Host;
        NetData.Players = Players;
        NetData.PlayerTurn = PlayerTurn;
        NetData.DeckCards = DeckCards.ToString();
        NetData.PlayerHands = DictToString<string,CardHand>(PlayerHands,null);
        NetData.PlayerTricks = PlayerTricks;
        NetData.GameStarted = bIsGameStarted;
        NetData.NumPlayers = NumPlayers;
        NetData.DeckID = DeckID;
        NetData.WantsMorePlayers = WantsMorePlayers;
        NetData.CurrentGameState = (int)CurrentGameState;
        
    }

    // pulls netdata into game object, overwriting values
    public void GetNetData()
    {
        Id = NetData.Id;

        GameID = NetData.GameID;
        Host = NetData.Host;
        Players = NetData.Players;
        PlayerTurn = NetData.PlayerTurn;
        DeckCards.FromString(NetData.DeckCards);
        PlayerHandsFromNetData( NetData.PlayerHands);
        PlayerTricks = NetData.PlayerTricks;
        bIsGameStarted = NetData.GameStarted;
        NumPlayers = NetData.NumPlayers;
        DeckID = NetData.DeckID;
        WantsMorePlayers = NetData.WantsMorePlayers;
        CurrentGameState = (GameState)NetData.CurrentGameState;
    }

    public void PlayerHandsFromNetData(string SaveFrom)
    {
        Debug.Log("Found PLAYERHANDS, converting");
        string TempString = SaveFrom;
        string PlayerID = "";
        string CardID = "";
        CardHand Hand;
        int Index = 0;
        // library for lookup - not a great solution but will work
        // for now.
        
        // if nothing in string, early out returning empty container
        if (TempString.Length <= 0)
            return;

        // while string > 0
        while (TempString.Length > 0)
        {
            // new hand object, will use at end
            Hand = new CardHand();
            // 3 phases
        
            // Phase 1 - get player name

            // get index of semicolon<;>
            Index = TempString.IndexOf(';');

            // mask off all chars before comma, this will be playerID
            PlayerID = TempString.Substring(0, Index);

            // cut that off front of string
            TempString = TempString.TrimStart(PlayerID.ToCharArray());

            // also cut semi
            TempString = TempString.TrimStart(';');

            // Phase 2 - get player's cards, separated by commas <,>
            // need a check to know when I've found the cards
            bool bAllCardsFound = false;
            // see if '$' is closer than ','
            var EndplayerIndex = TempString.IndexOf('$');
            var NextCardIndex = TempString.IndexOf(',');

            // if endplayer is closer than next card, this player has no cards, 
            // if nextcardindex == -1, no more cards at all for player
            if (EndplayerIndex < NextCardIndex || 
                NextCardIndex == -1 )
            {
            
                // cut off $ as it will disrupt next loop if another player has cards
                TempString = TempString.TrimStart('$');
                // set break out bool to true so we go to next player
                bAllCardsFound = true;

                // add this player and hand to the dictionary
                PlayerHands.Add(PlayerID, Hand);
            }

            
            
            // nested loop isn't pretty but well its best way I can think of at moment.
            while (bAllCardsFound == false)
            {
                // get index of comma
                // get index of comma<,>
                Index = TempString.IndexOf(',');
                
                // mask off all chars before comma, this will be playerID
                CardID = TempString.Substring(0, Index);

                // cut that off front of string
                TempString = TempString.TrimStart(CardID.ToCharArray());

                // also cut comma
                TempString = TempString.TrimStart(',');

                // build card and add it to hand
                Card newCard = DeckCards.GetCardFromLibrary(Convert.ToInt32(CardID));
                
                // put card near hand
                newCard.transform.position = Hand.m_vHandPosition;
                
                // add it to hand
                Hand.AddCard(newCard);

                // Phase 3 - look for $ which lets me know this player done
                Index = TempString.IndexOf('$');
                // if $ is the next char in the sequence, I'm done w/ current player
                if (Index == 0)
                {
                    // cut the $
                    TempString = TempString.TrimStart('$');
                    // set break out bool to true so we go to next player
                    bAllCardsFound = true;

                    // add this player and hand to the dictionary
                    PlayerHands.Add(PlayerID, Hand);
                }
        
            } // end nexted while
        
        } // end outter while

        
    }

    public Dictionary<string, int> PlayerTricksFromNetData(string SaveFrom)
    {
        return null;
    }

    void Start()
    {

    }

    void Update()
    {
        if (bGameIsActive)
            PlayGame();
    }

    // in-game logic. 
    public void PlayGame()
    {
        // check state of the game
        // really should do state machine - will do for next game.
        switch (CurrentGameState)
        {
            case(GameState.eWaitingForPlayers):
                WaitingForPlayers();
                break;

            case(GameState.eGameFullWaitingToStart):
                GameFullWaitingToStart();
                break;

            case(GameState.eGameStarted):
                GameStarted();
                break;

            case(GameState.eGameFinished):
                GameFinished();
                break;
        }

        
    }

    void WaitingForPlayers()
    {

    }

    void GameFullWaitingToStart()
    {

    }

    void GameStarted()
    {
        // 
    }

    void GameFinished()
    {

    }


    public void DealCards()
    {
        // TODO: Remove this, temp hand data for testing
        for (int i = 0; i < 5; ++i)
        {
            //// pop card off deck
            //CardDef DealtCard = GameToJoin.DeckCards.Pop();

            //GameObject newObj = new GameObject();

            //newObj.name = "Card";

            //Card newCard = newObj.AddComponent(typeof(Card)) as Card;

            //newCard.Definition = DealtCard;

            //newObj.transform.parent = GameToJoin.DeckCards.transform;

            //newCard.TryBuild();

            //// add to hand
            //NewPlayerHand.AddCard(newCard);
        }
    }

    void OnGUI()
    {
        // GUI FOR ALL STATES
        if (bGameIsActive)
        {
            // calc sizes - divides screen into 4 quarters
            var Width = MainGameVars.m_fScreenWidth * MainGameVars.m_fButtonWidth;
            var Height = MainGameVars.m_fScreenHeight * MainGameVars.m_fButtonHeight;
            var TopLeftX = MainGameVars.m_fScreenWidth / 2 - Width;
            var TopLeftY = MainGameVars.m_fScreenHeight / 10.0f;

            // let you go back to main menu
            if (GUI.Button(new Rect(0, 0, Width, Height), "Main Menu"))
            {
                // set game to inactive
                bGameIsActive = false;
                MainGameVars.bIsInGame = false;
            }

            // STATE-BASED GUI
            switch (CurrentGameState)
            {
                case (GameState.eWaitingForPlayers):
                    WaitingForPlayersGUI();
                    break;

                case (GameState.eGameFullWaitingToStart):
                    GameFullWaitingToStartGUI();
                    break;

                case (GameState.eGameStarted):
                    GameStartedGUI();
                    break;

                case (GameState.eGameFinished):
                    GameFinishedGUI();
                    break;
            }
        }

        
    }

    void WaitingForPlayersGUI()
    {
        // if I'm the creator of the game I can choose to start the game regardless of if it is full or not
        if (Host == MainGameVars.PlayerID)
        {
            // calc sizes - divides screen into 4 quarters
            var Width = MainGameVars.m_fScreenWidth * MainGameVars.m_fButtonWidth;
            var Height = MainGameVars.m_fScreenHeight * MainGameVars.m_fButtonHeight;
            var TopLeftX = MainGameVars.m_fScreenWidth / 2 - Width ;
            var TopLeftY = MainGameVars.m_fScreenHeight / 5.0f ;

            // Make a background box
            GUI.Box(new Rect(TopLeftX, TopLeftY, Width * 2, Height * 2), "Game Options");

            // can't very well play against yourself - so make sure there's more than 1 player
            if(Players.Count > 1)
            {

                TopLeftX = MainGameVars.m_fScreenWidth / 2 - Width / 2;
                TopLeftY = MainGameVars.m_fScreenHeight * (3.0f/10.0f);
                // Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
                if (GUI.Button(new Rect(TopLeftX, TopLeftY, Width, Height), "Start Game"))
                {
                    CurrentGameState = GameState.eGameStarted;
                    bIsGameStarted = true;
                    GameStartUp();

                    // set new net data
                    SetNetData();
                
                    // save state
                    NetData.ToParseObject("GoFishGame").SaveAsync().ContinueWith(task =>
                    {
                        if (task.IsCanceled)
                        {
                            // the save was cancelled.
                            Debug.Log("Save cancelled.");
                        }
                        else if (task.IsFaulted)
                        {
                            AggregateException exception = task.Exception;
                            Debug.Log(exception);
                        }
                        else
                        {
                            // the object was saved successfully.
                            Debug.Log("Save Successful, game started");

                        }

                        MainGameVars.m_bAppIsBusy = false;

                    });

                }// end start game button

            } // end if players > 0 

        }
        // not host, just show text saying waiting to start game
        else
        {

        }
    }

    void GameFullWaitingToStartGUI()
    {

    }

    void GameStartedGUI()
    {

    }

    void GameFinishedGUI()
    {

    }

    void GameStartUp()
    {
        // shuffle just in case
        DeckCards.Shuffle();

        // deal one card to each person 5 times - assuming 3+ players
        for (int i = 0; i < 5; ++i)
        {
            // deal cards to the players
            foreach (var player in Players)
            {

                // pop card off deck
                CardDef DealtCard = DeckCards.Pop();

                GameObject newObj = new GameObject();

                newObj.name = "Card";

                Card newCard = newObj.AddComponent(typeof(Card)) as Card;

                newCard.Definition = DealtCard;

                newObj.transform.parent = DeckCards.transform;

                newCard.TryBuild();

                // add to hand
                PlayerHands[player].AddCard(newCard);

            }

        }

        // set net data
        SetNetData();

        MainGameVars.m_bAppIsBusy = true;

        Debug.Log("Cards Dealt, saving state.");

        // save changes
        NetData.ToParseObject("GoFishGame").SaveAsync().ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    // the save was cancelled.
                    Debug.Log("Save cancelled.");
                }
                else if (task.IsFaulted)
                {
                    AggregateException exception = task.Exception;
                    Debug.Log(exception);
                }
                else
                {
                    // the object was saved successfully.
                    Debug.Log("Save Successful");

                }

                MainGameVars.m_bAppIsBusy = false;
            });
    }

    

}

public class GoFishGameNetData
{
    // unique id set by service
    public string Id { get; set; }

    // unique id of the game based on host name and uniqueid
    public string GameID { get; set; }

    // host of the game
    public string Host { get; set; }

    // all players of the game - limiting to 6 for now
    public IList<string> Players { get; set; }

    // who's turn is it 
    public string PlayerTurn { get; set; }

    //what's left in the deck
    public string DeckCards { get; set; }

    // what cards everyone has in their hand - SERIALIZED BY HAND
    // is a Dictionary<string, CardHand>
    public string PlayerHands { get; set; }

    // scoring - who has how many tricks
    public IDictionary<string,int> PlayerTricks { get; set; }

    // game started or not
    public bool GameStarted { get; set; }

    // number of players in a game
    public int NumPlayers { get; set; }

    // deck ID (from store)
    public string DeckID { get; set; }

    // determines if game is looking for more players
    public bool WantsMorePlayers { get; set; }

    // current game state
    public int CurrentGameState { get; set; }

    // parse data kept with the object
    private ParseObject ParseData { get; set; }

    public ParseObject ToParseObject(string ObjectName)
    {
        // create parseobject if doesn't already exist
        if (ParseData == null)
        {
            Debug.Log("Creating new Parse Object");
            ParseData = new ParseObject(ObjectName);

        }
        

        // set fields
        foreach (var propertyInfo in this.GetType()
                                .GetProperties(
                                        BindingFlags.Public
                                        | BindingFlags.Instance))
        {
            // do stuff here
            ParseData[propertyInfo.Name] = propertyInfo.GetValue(this, null);
        }


        return ParseData;
    }

    public void FromParseObject(ParseObject QueryResult)
    {

        // TODO: Find better way to do this. this way sucks

        // unique id set by service
        Id = QueryResult.ObjectId;         

        GameID = QueryResult.Get<string>("GameID");

        Host = QueryResult.Get<string>("Host");

		Players = QueryResult.Get<IList<string>>("Players");

        PlayerTurn = QueryResult.Get<string>("PlayerTurn");

        DeckCards = QueryResult.Get<string>("DeckCards");

        PlayerHands = QueryResult.Get<string>("PlayerHands");

        PlayerTricks = QueryResult.Get<IDictionary<string,int> >("PlayerTricks");

        GameStarted = QueryResult.Get<bool>("GameStarted");

        NumPlayers = QueryResult.Get<int>("NumPlayers");

        DeckID = QueryResult.Get<string>("DeckID");

        WantsMorePlayers = QueryResult.Get<bool>("WantsMorePlayers");

        CurrentGameState = QueryResult.Get<int>("CurrentGameState");

        // probably should save this. duh
        ParseData = QueryResult;

    }

    public string GetID()
    {
        return ParseData.ObjectId;
    }

}

