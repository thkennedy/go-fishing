﻿using UnityEngine;
using System.Collections;

public class MainMenuButtons : MonoBehaviour {

    public GUISkin mMainMenuSkin;

    public void OnGUI()
    {
        // load the skin
        GUI.skin = mMainMenuSkin;

        // create a button large as the screen so a touch anywhere goes to the game
        if (GUI.Button(new Rect(0, 0, Screen.width, Screen.height),""))
        {
            //transition scene
            Application.LoadLevel("Go Fishing");
        }
     
    }

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
