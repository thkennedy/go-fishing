using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
/*
public class AzureMobileServices : MonoBehaviour 
{

	//public AzureMobileServicesSDK MobileServices;
    // deck prefab
    public DeckStandard StandardDeck;
    // game prefab
    public GoFishGame GamePrefab;

	// list of games I'm playing
    public List<GoFishGame> m_lGameList = new List<GoFishGame>();

    // recheck rate for waiting on services to finish
    public const float APP_SERVICE_REFRESH_RATE = 1.0f;

    // letting outside entities know if app is busy
    public bool m_bAppIsBusy;

    // Initialize network aspect
    public void InitializeAzure()
    {
        if (MobileServices.IsOnline())
            MobileServices.InitMobileService("https://gofishing.azure-mobile.net/", "QtSokaYBuuGlSjgknmUvpMngogRGRJ39", "Game");
        else
            StartCoroutine(TryToInitAzure());
    }

    // coroutine to continually try to initialize Azure
    IEnumerator TryToInitAzure()
    {
        while (true)
        {
            if (!MobileServices.IsOnline())
            {
                yield return new WaitForSeconds(1);
            }
            else
            {
                MobileServices.InitMobileService("https://gofishing.azure-mobile.net/", "QtSokaYBuuGlSjgknmUvpMngogRGRJ39", "Game");
                yield break;
            }
        }
    }
    // Refresh Game List
    public void RefreshGameList()
    {
        // query database for all entries where I am listed as a player
        MobileServices.QueryDB("players", SystemInfo.deviceUniqueIdentifier);
        // wait for query to finish
        StartCoroutine(WaitForActionToFinish());
        // if there are any records
        if (MobileServices.mRecords != null)
        {
            Debug.Log("Found Game(s) I'm a part of, getting them now");
            // temp container to get games
            List<Dictionary<string, string>> Games = new List<Dictionary<string, string>>();
            Games = MobileServices.mRecords;
            Debug.Log("Found " + Games.Count + " Games I'm a part of");
            GoFishGame TempGame;
            // flush current game list
            m_lGameList.Clear();

            foreach (Dictionary<string, string> entry in Games)
            {

                // instantiate new game
                TempGame = Instantiate(GamePrefab) as GoFishGame;
                // convert entry from strings to a game object
                TempGame.ConvertToGameObject(entry);
                // add to list of games
                m_lGameList.Add(TempGame);
            }
        }
        else
        {
            Debug.Log("No records Found, create a game");
        }
        

        

    }
    // Create New Game
    public void CreateNewGame()
    {
        // create new game
        GoFishGame NewGame = Instantiate(GamePrefab) as GoFishGame;

        // set beginning vars
        // give game a semi-unique id based on device + game number - may get rid of this later
        NewGame.GameID = SystemInfo.deviceUniqueIdentifier + m_lGameList.Count;
        // host of the game - will later use name/fb/etc + this identifier
        NewGame.Host = SystemInfo.deviceUniqueIdentifier;
        // player turn will be the person who joins after the host - however, can't assign it
        // to someone who isn't here yet - set to host for now. When game begins will rand first player
        NewGame.PlayerTurn = NewGame.Host;
               
        // create deck
        DeckStandard CardDeck = Instantiate(StandardDeck) as DeckStandard;

        CardDeck.Initialize();

        NewGame.DeckCards = CardDeck;
        // shuffle the deck
        NewGame.DeckCards.Shuffle();
        // hands haven't been dealt yet, but create container
        NewGame.PlayerHands = new Dictionary<string,CardHand>();
        // set number of tricks for host
        NewGame.PlayerTricks = new Dictionary<string, int>();
        // set game started to false
        NewGame.GameStarted = "false";
        // set numplayers to 1 
        NewGame.NumPlayers = 1.ToString();
        //test

        // join the game we just made
        JoinGame(NewGame);

        // send game to Azure
        MobileServices.Insert( NewGame.GetTableVersionOfObject());

        StartCoroutine(WaitForActionToFinish());
        
    }

    IEnumerator WaitForActionToFinish()
    {
        while (true)
        {
            Debug.Log("Waiting for Service to finish.");
            // if service isn't busy, its done so move on
            if (MobileServices.IsNotBusy())
            {
                m_bAppIsBusy = false;
                yield break;
            }

            m_bAppIsBusy = true;

            // TODO: DISABLE UI AND DO LOADING LOGIC HERE

            yield return new WaitForSeconds(APP_SERVICE_REFRESH_RATE);
        }
    }
    // UpdateGame
    public void UpdateGame()
    {

    }
    // Search For Open Games
    public void SearchOpenGames()
    {

    }
    // join a game that is currently open
    public void JoinGame(GoFishGame GameToJoin)
    {
        // add self to players list
        GameToJoin.Players.Add(SystemInfo.deviceUniqueIdentifier);

        // create a player hand and add to game
        CardHand NewPlayerHand = new CardHand();

        GameToJoin.PlayerHands.Add(SystemInfo.deviceUniqueIdentifier, NewPlayerHand);

        //LOCAL DATA REMOVE LATER
       // GameToJoin.PlayerHands[0].AddCard(GameToJoin.DeckCards.Pop);

		m_lGameList.Add (GameToJoin);


        
    }

	// Use this for initialization
	void Start () 
	{

		

			
	}
	
	// Update is called once per frame
	void Update () 
	{

		
		// update online status
		MobileServices.IsOnline ();


	}
}


*/