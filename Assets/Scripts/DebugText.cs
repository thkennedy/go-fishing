﻿using UnityEngine;
using System.Collections;
using Bitrave.Azure;

public class DebugText : MonoBehaviour {

	public AzureMobileServices AzureObject;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        // This displays Debug text for the game
        this.guiText.text = "Screen Resolution: X= " + Screen.width + " Y= " + Screen.height +
            "\nDeltaTime: " + Time.deltaTime;
	
	
	}
}
