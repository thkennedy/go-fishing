﻿using UnityEngine;
using System.Collections;

public class FacebookManager : MonoBehaviour {

    public bool bIsBusy;

    public Texture2D FBButtonLoginTexture;

    public Texture2D FBButton;

    // so I can set the playerID when its avail.
    public MainGameLogic MainGameVars;

	// Use this for initialization
	void Start () {
        Debug.Log("Initializing FB SDK");
        enabled = false;
        FB.Init(SetInit, OnHideUnity);

        // default to login
        FBButton = FBButtonLoginTexture;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

   

    public void Login()
    {
        if (enabled == true)
        {
            FB.Login("email", AuthCallback);
            bIsBusy = true;
        }
        else
        {
            Debug.Log("FB Not initialized, please wait");
        }
    }

    void AuthCallback(FBResult result)
    {
        
        if (FB.IsLoggedIn)
        {
            Debug.Log(FB.UserId);
            MainGameVars.PlayerID = FB.UserId;
        }
        else
        {
            Debug.Log("User cancelled login");

        }

        bIsBusy = false;
    }


    private void SetInit()
    {
        enabled = true;

        Debug.Log("FB Initialized");
        // "enabled" is a magic global; this lets us wait for FB before we start rendering
        if (FB.IsLoggedIn)
            MainGameVars.PlayerID = FB.UserId;
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // start the game back up - we're getting focus again
            Time.timeScale = 1;
        }
    }
}
