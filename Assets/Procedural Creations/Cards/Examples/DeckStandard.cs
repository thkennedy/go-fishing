using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DeckStandard : CardDeck
{
	public CardAtlas Atlas;
	public CardStock Stock;
    CardDeckLibrary m_Library;

	public override void Initialize()
	{

		Debug.Log("Atlas = "+Atlas.name);
        // create library
        m_Library = new CardDeckLibrary();

        // get library for standard deck
        DeckLibrary = new Dictionary<int,CardDef>();
        DeckLibrary = m_Library.GetStandard52CardDeck();
		m_itemList = new DeckItem[52];
        int i = 0;

        foreach (var entry in DeckLibrary)
		{
			DeckItem item = new DeckItem();
            entry.Value.Atlas = Atlas;
            entry.Value.Stock = Stock;
			item.Count = 1;
			item.Card = entry.Value;
			m_itemList[i++] = item;

            // add to list of cards
            m_cards.Add(entry.Value);
		}
	}

    /** creates a deck with only the cards passed in by the string
     * Assuming this was taken from a serialized piece of data from
     * a server and has the unique cardID system
     * */
    public void InitStandard52CardDeckFromString(string StringData)
    {
        // get library for standard deck
        DeckLibrary = new Dictionary<int, CardDef>();
        // creating the deck for ease of adding
        List<CardDef> TempDeckItems = new List <CardDef>();

        string TempString = StringData;
        string MaskString = "";
        int Index = 0;
        int CardID = -1;

        // create library if it doesn't exist
        if (m_Library == null)
        {
            m_Library = new CardDeckLibrary();
            
        }

         // get library for standard deck
        DeckLibrary = m_Library.GetStandard52CardDeck();
       

        while (TempString.Length > 0)
        {
            // get index of comma<,>
            Index = TempString.IndexOf(',');

			// LAST ENTRY WON'T HAVE A COMMA AFTER IT
			if (Index == -1)
			{
				MaskString = TempString;
			}
			else
			{
				// this will be the mask
	            MaskString = TempString.Substring(0,Index);
			}

            // cut that off front of string
            TempString = TempString.TrimStart(MaskString.ToCharArray());

            // also cut comma - won't do anything if no comma
            TempString = TempString.Trim(',');

            // convert mask into int to look up card
            CardID = Convert.ToInt32(MaskString);

            // verify that the key is valid - key not valid, gets skipped.
            if (DeckLibrary[CardID] != null)
            {
                DeckLibrary[CardID].Atlas = Atlas;
                DeckLibrary[CardID].Stock = Stock;
                TempDeckItems.Add(DeckLibrary[CardID]);
            }



        } // end while loop

        // count variable so we're not querying deckitems all the time
        int DeckItemsCount = TempDeckItems.Count;
        // init item array
        m_itemList = new DeckItem[DeckItemsCount];
       
        // Card item creation
        DeckItem item = new DeckItem();
                       
        for (int i = 0; i < DeckItemsCount; ++i)
        {
			item = new DeckItem();
            item.Count = 1;
            item.Card = TempDeckItems[i];
            m_itemList[i] = item;

            // add to list of cards
            m_cards.Add(TempDeckItems[i]);
        }
        
    }

    public Card GetCardFromLibrary(int CardID)
    {
        // get definition from library
        CardDef Definition = DeckLibrary[CardID];
        // create new game object
        GameObject newObj = new GameObject();
        newObj.name = "Card";
        // creating card itself to return
        Card newCard = newObj.AddComponent(typeof(Card)) as Card;
        // get the definition
        newCard.Definition = Definition;

        newCard.TryBuild();

        return newCard;
    }

    public override void FromString(string Data)
    {
        // init just in case hasn't been done already
        Initialize();

        InitStandard52CardDeckFromString(Data);
    }


}
