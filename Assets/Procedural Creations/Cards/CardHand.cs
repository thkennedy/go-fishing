﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * This is the hand class for card games - it is composed simply of an owner and 
 * set of cards along with some limit params
 * */

public enum HandArrangement
{
    eStraightHand,
    eCurvedHand
};

public enum HandSpacing
{
    eFullWidth,
    eCompact,

};

[System.Serializable]
public class CardHand// : MonoBehaviour 
{

    // who is the owner of this hand?
    public string m_sHandOwner;

    // what cards are in this hand
    public List<Card> m_lCardsInHand;

    // how large is the hand (how many cards)
    public int m_iMaxHandSize;

    // hand's position in world space
    public Vector3 m_vHandPosition;

    // hand's arrangement
    public HandArrangement m_eArrangement;

    // hand's spacing
    public HandSpacing m_eSpacing;

    // how quickly the cards transition to where they're going.
    public float CardTransitionTime;

    // default constructor
    public CardHand()
    {
        m_sHandOwner = "NoOne";
        m_lCardsInHand = new List<Card>();
        m_iMaxHandSize = 999;
        m_eArrangement = HandArrangement.eStraightHand;
        m_eSpacing = HandSpacing.eFullWidth;
    }

    // para constructor
    public CardHand(string HandOwner, List<Card> CardsInHand, int MaxHandSize)
    {
        m_sHandOwner = HandOwner;
        m_lCardsInHand = new List<Card>();
        m_lCardsInHand = CardsInHand;
        m_iMaxHandSize = MaxHandSize;

    }

    // adds a card to the hand
    public void AddCard(Card NewCard)
    {
        m_lCardsInHand.Add(NewCard);

        UpdateCardArrangement();
    }

    public void RemoveCard(Card RemovedCard)
    {
        m_lCardsInHand.Remove(m_lCardsInHand.Find(x => x.Equals(RemovedCard)));

        UpdateCardArrangement();
    }

    // Use this for initialization
	void Start () 
    {
        CardTransitionTime = 1.0f;
	}

    // updates card arrangement - accounts for new cards and removed cards
    public void UpdateCardArrangement()
    {
        float MaxCardSpace;
        float MiddleOfCardSpace;
        Vector3 TargetLocation;
        for (int i = 0; i < m_lCardsInHand.Count ; ++i)
        {
            
            switch (m_eSpacing)
            {
                // sets cards overlapping slightly - need to make smarter to take into account size of card
                case HandSpacing.eCompact:
                    
                break;
                // sets spacing further apart, using screen width
                case HandSpacing.eFullWidth:
                    // find max card space
                    MaxCardSpace = Screen.width / m_lCardsInHand.Count;
                    // find middle of that space
                    MiddleOfCardSpace = MaxCardSpace / 2;
                    // set our target location
                    TargetLocation = new Vector3(MaxCardSpace * i - MiddleOfCardSpace, 0, 1);
                    // move the card there
                    m_lCardsInHand[i].SetFlyTarget(m_lCardsInHand[i].transform.position, TargetLocation, CardTransitionTime);

                break;

                default:

                break;

            }

        }   
    }
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    override public string ToString()
    {
        string ReturnString = "";

        for (int i = 0; i < m_lCardsInHand.Count; ++i)
        {
            ReturnString += m_lCardsInHand[i].ToString() + ',';
        }

        return ReturnString;
    }

    public int GetNumberOfCardsInHand()
    {
        return m_lCardsInHand.Count;
    }
}
