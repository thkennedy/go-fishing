using UnityEngine;
using System.Collections;

public class CardDefinition : MonoBehaviour
{
	public CardDef Data;
}

[System.Serializable]
public class CardDef
{
	public CardAtlas Atlas;
	public CardStock Stock;
    public int m_iCardID; // deck-unique card id
	public string Text;
	public string Symbol; // Atlas shape name
	public int Pattern;
	public string Image;
	public bool FullImage;
	
	public CardDef(CardAtlas atlas, CardStock stock, string text, string symbol, int pattern)
	{
		Atlas = atlas;
		Stock = stock;
		Text = text;
		Symbol = symbol;
		Pattern = pattern;
	}

    public CardDef(CardAtlas atlas, CardStock stock, string text, string symbol, int pattern, int CID)
    {
        Atlas = atlas;
        Stock = stock;
        Text = text;
        Symbol = symbol;
        Pattern = pattern;
        m_iCardID = CID;
    }

    // puts the text(a-k) and symbol(suit) in a comma separated string
    override public string ToString()
    {
        return m_iCardID.ToString();
    }

    
}