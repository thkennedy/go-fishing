using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CardDeck : MonoBehaviour
{
    [System.Serializable]
    public class DeckItem
    {
        public int Count = 1;
        public CardDef Card;

        // string-ifies the deck item
        override public string ToString()
        {
            string ReturnString = "";

            ReturnString += Count.ToString() + "," + Card.ToString();

            return ReturnString;
        }
    }

    protected DeckItem[] m_itemList;
    protected Dictionary<int, CardDef> DeckLibrary;

    // List of cards in live deck
    protected List<CardDef> m_cards = new List<CardDef>();

    public virtual void Initialize()
    {
    }

    public void Reset()
    {
        m_cards.Clear();

        foreach (DeckItem item in m_itemList)
        {
            for (int i = 0; i < item.Count; ++i)
            {
                m_cards.Add(item.Card);
            }
        }
    }

    public void Shuffle()
    {
        var CardCount = m_cards.Count;

        for (int i = 0; i < CardCount; ++i)
        {
            int other = UnityEngine.Random.Range(0, m_cards.Count);
            if (other != i)
            {
                CardDef swap = m_cards[i];
                m_cards[i] = m_cards[other];
                m_cards[other] = swap;
            }
        }
    }

    public CardDef Pop()
    {
        int last = m_cards.Count - 1;
        if (last >= 0)
        {
            CardDef result = m_cards[last];
            m_cards.RemoveAt(last);
            return result;
        }
        return null;
    }

    override public string ToString()
    {
        string ReturnString = "";
        var CardCount = m_cards.Count;

        for (int i = 0; i < CardCount; ++i)
        {
            ReturnString += m_cards[i].ToString() + ",";
        }

        // trim last comma
        ReturnString = ReturnString.TrimEnd(',');

        return ReturnString;
    }

    public virtual void FromString(string Data)
    {
        // init just in case hasn't been done already
        Initialize();
        
    }

}

   