﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// place to keep all currently available decks and their CID's
// TODO: Possibly conver to singleton for easier access.
// Why : Deck needs, hand needs, possibly others

public class CardDeckLibrary  
{
    // available deck types - currently in the app, but will later be gotten from
    //  a server. Only init these as they are called to save memory
    public Dictionary<int, CardDef> Standard52CardDeck;

    public Dictionary<int, CardDef> GetStandard52CardDeck()
    {
        if (Standard52CardDeck == null)
            {
                BuildStandard52CardDeck();
            }

        return Standard52CardDeck;
    }

    //public Card GetCardFromLibrary(int CardID, Dictionary<int, CardDef> LibraryToSearch)
    //{
    //    // get definition from library
    //    CardDef Definition = LibraryToSearch[CardID];
    //    // create new game object
    //    GameObject newObj = new GameObject();
    //    newObj.name = "Card";
    //    // creating card itself to return
    //    Card newCard = newObj.AddComponent(typeof(Card)) as Card;
    //    // get the definition
    //    newCard.Definition = Definition;

    //    newCard.TryBuild();

    //    return newCard;
    //}


    public void BuildStandard52CardDeck()
    {
        // static so that only the ones that are called are ever created
        Standard52CardDeck = new Dictionary<int,CardDef>();
        
        string [] suits = new string[]{"Heart","Spade","Diamond","Club"};
		string [] prefixes = new string[]{"H-","S-","D-","C-"};
		// CID incrementor
        int j = 1;
		for (int i=0; i<4; ++i)
		{
			int ii = i*13;
			string symbol = suits[i];
            Standard52CardDeck.Add(j, new CardDef(null, null, "A", symbol, 1, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "2", symbol, 2, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "3", symbol, 3, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "4", symbol, 4, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "5", symbol, 5, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "6", symbol, 6, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "7", symbol, 7, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "8", symbol, 8, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "9", symbol, 9, j++));
            Standard52CardDeck.Add(j, new CardDef(null, null, "10", symbol, 10, j++));
			string prefix = prefixes[i];
			CardDef jj = new CardDef(null,null,"J",symbol,0,j);
			jj.Image = prefix+"Jack";
			Standard52CardDeck.Add(j++, jj);
			CardDef qq = new CardDef(null,null,"Q",symbol,0,j);
			qq.Image = prefix+"Queen";
			Standard52CardDeck.Add(j++, qq );
			CardDef kk = new CardDef(null,null,"K",symbol,0,j);
			kk.Image = prefix+"King";
			Standard52CardDeck.Add(j++, kk );

           
		}
    }
}
